# README #

This repo is an attempt to collect apps and their screenshots.


# Ecommerce Android Apps

## Flipkart Apk

Flipkart app screenshots can be found and downloaded from [flipkart apk](https://apkmozo.com/apps/flipkart-online-shopping-app/com.flipkart.android)

## Amazon Apk

Amazon app screenshots can be found and downloaded from [amazon apk](https://apkmozo.com/apps/amazon-shopping-upi-money-transfer-bill-payment/in.amazon.mShop.android.shopping)

## Indiamart Apk

Flipkart app screenshots can be found and downloaded from [indiamart apk](https://apkmozo.com/apps/indiamart/com.indiamart.m)

## Myntra Apk

Flipkart app screenshots can be found and downloaded from [myntra apk](https://apkmozo.com/apps/myntra-online-shopping-app-shop-fashion-more/com.myntra.android)

## Snapdeal Apk

Flipkart app screenshots can be found and downloaded from [snapdeal apk](https://apkmozo.com/apps/snapdeal-online-shopping-app/com.snapdeal.main)

